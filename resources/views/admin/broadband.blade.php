<!DOCTYPE html>
<head>
   <title>Spider Management System | Home</title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
      Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
   <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
   <!-- bootstrap-css -->
   <link rel="stylesheet" href="../css/bootstrap.min.css" >
   <!-- //bootstrap-css -->
   <!-- Custom CSS -->
   <link href="../css/style.css" rel='stylesheet' type='text/css' />
   <link href="../css/style-responsive.css" rel="stylesheet"/>
   <!-- font CSS -->
   <link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   <!-- font-awesome icons -->
   <link rel="stylesheet" href="../css/font.css" type="text/css"/>
   <link href="../css/font-awesome.css" rel="stylesheet">
   <!-- //font-awesome icons -->
   <script src="../js/jquery2.0.3.min.js"></script>
</head>
<body>
   <section id="container">
      <!--header start-->
      <header class="header fixed-top clearfix">
         <!--logo start-->
         <div class="brand">
            <a href="index.html" class="logo">
            Spider
            </a>
            <div class="sidebar-toggle-box">
               <div class="fa fa-bars"></div>
            </div>
         </div>
         <!--logo end-->
         <div class="top-nav clearfix">
            <!--search & user info start-->
            <ul class="nav pull-right top-menu">
               <li>
                  <input type="text" class="form-control search" placeholder=" Search">
               </li>
               <!-- user login dropdown start-->
               <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                  <img alt="" src="../images/T2A.jpg">
                  <span class="username">{{ Auth::user()->name }}</span>
                  <b class="caret"></b>
                  </a>
                  <ul class="dropdown-menu extended logout">
                     <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>
                     <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                     <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i> Log Out </a></li>
                  </ul>
               </li>
               <!-- user login dropdown end -->
            </ul>
            <!--search & user info end-->
         </div>
      </header>
      <!--header end-->
      <!--sidebar start-->
      @include('admin.product_sidebar')
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
         <section class="wrapper">
            <div class="table-agile-info">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     Wifi Broadband Management 
                  </div>
                  <div class="row">
                     <div class="col-lg-12">
                        <section class="panel">
                           <!-- <header class="panel-heading">
                              Modal form
                              </header> -->
                           <div class="panel-body">
                              <div class="position-center ">
                                 <div class="text-center">
                                    <a href="#myModal" data-toggle="modal" class="btn btn-success">
                                    New Buy
                                    </a>
                                    <!-- <a href="#myModal-1" data-toggle="modal" class="btn btn-warning">
                                       Form in Modal 2
                                       </a>
                                       <a href="#myModal-2" data-toggle="modal" class="btn btn-danger">
                                       Form in Modal 3
                                       </a> -->
                                 </div>
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                             <h4 class="modal-title">New Register Broadband</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form role="form" method="POST" action="{{ url('product/new-broadband') }}">
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Broadband Type : </label>
                                                   <select class="input-sm form-control w-sm inline v-middle">
                                                        <option>Please Choose</option>
                                                        <option>Ooredoo Supernet</option>
                                                        <option>Ooredoo BiB Sim Only</option>
                                                        <option>Telenor</option>
                                                   </select>
                                                </div>

                                                <div class="form-group">
                                                   <label>Date :</label>
                                                   <input type="date" class="form-control" placeholder=".col-md-4" style="width: 32%;">
                                                </div>

                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Customer Name :</label>
                                                   <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter email" style="width: 50%;">
                                                </div>
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Customer Phone No :</label>
                                                   <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter email" style="width: 50%;">
                                                </div>
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Router No :</label>
                                                   <input type="email" class="form-control" id="exampleInputEmail3" placeholder="995 *****" style="width: 50%;">
                                                </div>
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">MME No : </label>
                                                   <input type="email" class="form-control" id="exampleInputEmail3" placeholder="899 *****" style="width: 50%;">
                                                </div>
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Charges : </label>
                                                   <input type="email" class="form-control" id="exampleInputEmail3" style="width: 50%;">
                                                </div>
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Remark : </label>
                                                   <textarea class="form-control " id="ccomment" name="comment"></textarea>
                                                </div>
                                             
                                                <button type="submit" class="btn btn-success">Register</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                             <h4 class="modal-title">Form Tittle</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form class="form-horizontal" role="form">
                                                <div class="form-group">
                                                   <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Email</label>
                                                   <div class="col-lg-10">
                                                      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Password</label>
                                                   <div class="col-lg-10">
                                                      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-lg-offset-2 col-lg-10">
                                                      <div class="checkbox">
                                                         <label>
                                                         <input type="checkbox"> Remember me
                                                         </label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="form-group">
                                                   <div class="col-lg-offset-2 col-lg-10">
                                                      <button type="submit" class="btn btn-default">Sign in</button>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
                                    <div class="modal-dialog">
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                             <h4 class="modal-title">Form Tittle</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form class="form-inline" role="form">
                                                <div class="form-group">
                                                   <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                   <input type="email" class="form-control sm-input" id="exampleInputEmail5" placeholder="Enter email">
                                                </div>
                                                <div class="form-group">
                                                   <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                   <input type="password" class="form-control sm-input" id="exampleInputPassword5" placeholder="Password">
                                                </div>
                                                <div class="checkbox">
                                                   <label>
                                                   <input type="checkbox"> Remember me
                                                   </label>
                                                </div>
                                                <button type="submit" class="btn btn-default">Sign in</button>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                     </div>
                  </div>
                  <div class="row w3-res-tb">
                     <div class="col-sm-5 m-b-xs">
                        <select class="input-sm form-control w-sm inline v-middle">
                           <option value="0">Select Choose</option>
                           <option value="1">New Buy Broadband</option>
                           <option value="2">Recharged Broadband</option>
                           <!-- <option value="2">Bulk edit</option>
                              <option value="3">Export</option> -->
                        </select>
                        <button class="btn btn-sm btn-default">Apply</button>                
                     </div>
                     <div class="col-sm-4">
                     </div>
                     <div class="col-sm-3">
                        <div class="input-group">
                           <input type="text" class="input-sm form-control" placeholder="Search">
                           <span class="input-group-btn">
                           <button class="btn btn-sm btn-default" type="button">Search!</button>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="table-responsive">
                     <table class="table table-striped b-t b-light">
                        <thead>
                           <tr>
                              <!-- <th style="width:20px;">
                                 <label class="i-checks m-b-none">
                                   <input type="checkbox"><i></i>
                                 </label>
                                 </th> -->
                              <th style="width: 103px;">Date</th>
                              <th>Customer Name</th>
                              <th>Customer Ph No.</th>
                              <th>Router No.</th>
                              <th>MME No.</th>
                              <th>Buy Date</th>
                              <th>Charges</th>
                              <th>Type</th>
                              <th>Remark</th>
                              <th style="width:30px;"></th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>21-07-2019</td>
                              <td>THU THU</td>
                              <td>09-795938304</td>
                              <td>995123456789</td>
                              <td>899123456789</td>
                              <td>21-07-2019</td>
                              <td>95,000</td>
                              <td>Ooredoo</td>
                              <td>Cashed</td>
                              <td>
                                 <a href="broad_edit.html" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i></a>
                                  
                                 <i class="fa fa-times text-danger text" onclick="deleteFunction()"></i>
                                 </a>
                              </td>
                           </tr>
                        </tbody>
                        <!-- <tbody>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Idrawfast prototype design prototype design prototype design prototype design prototype design</td>
                             <td><span class="text-ellipsis">{item.PrHelpText1}</span></td>
                             <td><span class="text-ellipsis">{item.PrHelpText1}</span></td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Formasa</td>
                             <td>8c</td>
                             <td>Jul 22, 2013</td>
                             <td>
                               <a href="" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Avatar system</td>
                             <td>15c</td>
                             <td>Jul 15, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Throwdown</td>
                             <td>4c</td>
                             <td>Jul 11, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Idrawfast</td>
                             <td>4c</td>
                             <td>Jul 7, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Formasa</td>
                             <td>8c</td>
                             <td>Jul 3, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Avatar system</td>
                             <td>15c</td>
                             <td>Jul 2, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           <tr>
                             <td><label class="i-checks m-b-none"><input type="checkbox" name="post[]"><i></i></label></td>
                             <td>Videodown</td>
                             <td>4c</td>
                             <td>Jul 1, 2013</td>
                             <td>
                               <a href="" class="active" ui-toggle-class=""><i class="fa fa-check text-success text-active"></i><i class="fa fa-times text-danger text"></i></a>
                             </td>
                           </tr>
                           </tbody> -->
                     </table>
                  </div>
                  <footer class="panel-footer">
                     <div class="row">
                        <div class="col-sm-5 text-center">
                           <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
                        </div>
                        <div class="col-sm-7 text-right text-center-xs">
                           <ul class="pagination pagination-sm m-t-none m-b-none">
                              <li><a href=""><i class="fa fa-chevron-left"></i></a></li>
                              <li><a href="">1</a></li>
                              <li><a href="">2</a></li>
                              <li><a href="">3</a></li>
                              <li><a href="">4</a></li>
                              <li><a href=""><i class="fa fa-chevron-right"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </footer>
               </div>
            </div>
         </section>
         <!-- footer -->
         <div class="footer">
            <div class="wthree-copyright">
               <p>© 2019 Spider. All rights reserved | Design by <a href="https://www.facebook.com/thuthuaung29/?modal=admin_todo_tour">T2A</a></p>
            </div>
         </div>
         <!-- / footer -->
      </section>
      <!--main content end-->
   </section>
   <script src="../js/bootstrap.js"></script>
   <script src="../js/jquery.dcjqaccordion.2.7.js"></script>
   <script src="../js/scripts.js"></script>
   <script src="../js/jquery.slimscroll.js"></script>
   <script src="../js/jquery.nicescroll.js"></script>
   <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
   <script src="../js/jquery.scrollTo.js"></script>
   <script>
   function deleteFunction() {
     var txt;
     var r = confirm("Are you sure you want to delete?");
     if (r == true) {
       txt = "You pressed OK!";
     } else {
       txt = "You pressed Cancel!";
     }
     document.getElementById("demo").innerHTML = txt;
   }
   </script>
</body>
</html>

